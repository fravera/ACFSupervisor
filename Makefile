BUILD_HOME:=$(shell pwd)/..
include $(XDAQ_ROOT)/$(XDAQ_VERSION)/config/mfAutoconf.rules
include $(XDAQ_ROOT)/$(XDAQ_VERSION)/config/mfDefs.$(XDAQ_OS)

ROOTHTTP := $(shell root-config --has-http)
ROOTVERSION := $(shell root-config --version)

##################################################
## check if Root has Http
##################################################
ifneq (,$(findstring yes,$(ROOTHTTP)))
	RootExtraLinkFlags= -lRHTTP
	RootExtraFlags=-D__HTTP__
else
	RootExtraLinkFlags=
	RootExtraFlags=
endif
##################################################
## check Root version
##################################################
ifneq (,$(findstring 6.,$(ROOTVERSION)))
	RootExtraFlags+=-D__ROOT6__
endif

#Project=Ph2_TkDAQ
Package=ACFSupervisor
Sources= ACFSupervisor.cc SupervisorGUI.cc ACFStateMachine.cc DataSender.cc version.cc XMLUtils.cc #LogReader.h

IncludeDirs = \
	 ${XERCESC_ROOT}/include \
	 ${BOOST_INCLUDE} \
     $(XDAQ_INC) \
     $(XDAQ_INC)/linux \
	 ${PH2ACF_ROOT} \
	 ${CACTUSINCLUDE}
#	 /usr/include

LibraryDirs = \
	 ${XERCESC_ROOT}/lib/ \
     $(XDAQ_ROOT)/$(XDAQ_VERSION)/lib \
	 ${PH2ACF_ROOT}/lib \
	 ${CACTUSLIB} \
	 ${BOOST_LIB} \
	 ${ROOTLIB} \
     $(XDAQ_LIB)
#	 /usr/lib64 
 
ExeternalObjects =

UserCCFlags = -g -fPIC -std=c++11 -Wcpp -Wno-unused-local-typedefs -Wno-reorder -O0 $(shell pkg-config --cflags libxml++-2.6) $(shell xslt-config --cflags) $(shell root-config --cflags)
#UserCCFlags = -g -O1 -fPIC -std=c++14 -w -Wall -pedantic -pthread -Wcpp -Wno-reorder -O0 $(shell pkg-config --cflags libxml++-2.6) $(shell xslt-config --cflags) $(shell root-config --cflags) $(RootExtraFlags)
#UserDynamicLinkFlags = ${LibraryPaths} $(shell pkg-config --libs libxml++-2.6) $(shell xslt-config --libs)  $(shell root-config --libs) -lxdaq2rc -lconfig -lPh2_Utils -lPh2_Description -lPh2_Interface -lPh2_System -lPh2_Tools $(RootExtraLinkFlags)
UserDynamicLinkFlags = ${LibraryPaths} $(shell pkg-config --libs libxml++-2.6) $(shell xslt-config --libs)  $(shell root-config --libs) -lxdaq2rc -lconfig -lPh2_Utils -lPh2_Description -lPh2_Interface -lPh2_System -lPh2_Tools $(RootExtraLinkFlags)

#$(LibraryPaths)

DynamicLibrary=ACFSupervisor

include $(XDAQ_ROOT)/$(XDAQ_VERSION)/config/Makefile.rules
include $(XDAQ_ROOT)/$(XDAQ_VERSION)/config/mfRPM.rules
